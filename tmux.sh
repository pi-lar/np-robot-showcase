#!/usr/bin/env bash
tmux attach -t showcase || (tmux new-session -t showcase \; \
    split-window -v \; \
    select-pane -U \; \
    split-window -h \; \
    select-pane -L \; \
    send-keys "printf '\033]2;%s\033\\' 'Robot htop'" C-m \; \
    send-keys 'ssh robo' C-m \; \
    send-keys 'sudo htop' C-m \; \
    select-pane -R \; \
    send-keys "printf '\033]2;%s\033\\' 'Demosystem htop'" C-m \; \
    send-keys 'ssh demo.neuropil.io' C-m \; \
    send-keys 'sudo htop' C-m \; \
    select-pane -D \; \
    split-window -v \; \
    select-pane -U \; \
    split-window -h \; \
    select-pane -L \; \
    send-keys "printf '\033]2;%s\033\\' 'Robot Log'" C-m \; \
    send-keys 'ssh robo' C-m \; \
    send-keys 'echo -n | sudo tee /app/makebot/supervisor_stdout.log' C-m \; \
    send-keys 'less --follow-name +F /app/makebot/supervisor_stdout.log' C-m \; \
    select-pane -R \; \
    send-keys "printf '\033]2;%s\033\\' 'Coordinator Log'" C-m \; \
    send-keys 'ssh demo.neuropil.io' C-m \; \
    send-keys 'echo -n | sudo tee /var/log/neuropil_robot_showcase_coordinator_stdout.log' C-m \; \
    send-keys 'less --follow-name +F /var/log/neuropil_robot_showcase_coordinator_stdout.log' C-m \; \
    split-window -v \; \
    send-keys "printf '\033]2;%s\033\\' 'Deviceclient Log'" C-m \; \
    send-keys 'ssh demo.neuropil.io' C-m \; \
    send-keys 'echo -n | sudo tee /var/log/neuropil_robot_showcase_deviceclient_stdout.log' C-m \; \
    send-keys 'less --follow-name +F /var/log/neuropil_robot_showcase_deviceclient_stdout.log' C-m \; \
    select-pane -D \; \
    split-window -v \; \
    select-pane -U \; \
    split-window -h \; \
    select-pane -L \; \
    send-keys "printf '\033]2;%s\033\\' 'Robotsystem control'" C-m \; \
    send-keys 'ssh robo' C-m \; \
    send-keys 'sudo supervisorctl stop makebot;sudo killall /app/makebot/.venv/bin/python;sudo supervisorctl start makebot' C-m \; \
    select-pane -R \; \
    send-keys "printf '\033]2;%s\033\\' 'Demosystem control'" C-m \; \
    send-keys 'ssh demo.neuropil.io' C-m \; \
    send-keys 'supervisorctl restart neuropil_robot_showcase:' C-m \; \
    select-pane -D \; \
)