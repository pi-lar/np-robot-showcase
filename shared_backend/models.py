from enum import IntEnum
from typing import List, Set, Dict, Optional, Union
from pydantic import BaseModel, Field, PrivateAttr
import time

class BodyGroup(IntEnum):
    OTHER                           = 0 # Anderes
    DRIVE                           = 1 # Raupenketten
    CLAW                            = 2 # Klaue
    ARM                             = 3 # Arm
    Camera                          = 4 # Kamera

class Status(BaseModel):
    time_of_alteration:float        = time.time()
    created_at:float                = Field(default_factory=time.time)

    def updateTime(self):
        self.time_of_alteration     = time.time()

class Action(BaseModel):
    id:str
    bodyGroup:BodyGroup
    client_id:Optional[str]         = None
    is_active:bool                  = False

class DeviceStatus(Status):
    id:str
    type:str
    coordinator_id:Union[str,None]  = None
    actions:Dict[(str,Action)]      = {}
    availableBodyGroups:List[BodyGroup]

class CoordinatorStatus(Status):
    id:str
    name:str
    intendedDeviceId:Optional[str]  = None

class ClientStatus(Status):
    id:str
    name:str

class ActionPermissionMessage(BaseModel):
    client_id:str

class RequestDeviceMessage(BaseModel):
    client_id:str
    bodyGroup:BodyGroup
    # clould be extended by device type etc.

class ClaimDeviceMessage(BaseModel):
    client_id:str
    device_id:str
    # clould be extended by device type etc.
