import secrets
from uuid import UUID, uuid4
from typing import List, Set, Dict, Optional, Union

from pydantic import BaseModel
from fastapi import HTTPException, APIRouter, Response, Depends

from .event import EventHook

from fastapi_sessions.backends.implementations import InMemoryBackend
from fastapi_sessions.session_verifier import SessionVerifier
from fastapi_sessions.frontends.implementations import SessionCookie, CookieParameters


class SessionData(BaseModel):
    node_id: Optional[str] = None
    name:str

cookie_params = CookieParameters(max_age=86400)

# Uses UUID
cookie = SessionCookie(
    cookie_name="neuropilrobotshowcase"+secrets.token_hex(16),
    identifier="general_verifier",
    auto_error=False,
    secret_key=secrets.token_hex(16),
    cookie_params=cookie_params,
)
backend = InMemoryBackend[UUID, SessionData]()


class BasicVerifier(SessionVerifier[UUID, SessionData]):
    def __init__(
        self,
        *,
        identifier: str,
        auto_error: bool,
        backend: InMemoryBackend[UUID, SessionData],
        auth_http_exception: HTTPException,
    ):
        self._identifier = identifier
        self._auto_error = auto_error
        self._backend = backend
        self._auth_http_exception = auth_http_exception

    @property
    def identifier(self):
        return self._identifier

    @property
    def backend(self):
        return self._backend

    @property
    def auto_error(self):
        return self._auto_error

    @property
    def auth_http_exception(self):
        return self._auth_http_exception

    def verify_session(self, model: SessionData) -> bool:
        """If the session exists, it is valid"""
        return True


verifier = BasicVerifier(
    identifier="general_verifier",
    auto_error=True,
    backend=backend,
    auth_http_exception=HTTPException(status_code=403, detail="invalid session"),
)

router = APIRouter()
evt_new_session = EventHook()
evt_del_session = EventHook()

@router.post("/create_session/{name}")
async def create_session(response: Response, name:str):
    session = uuid4()
    data = SessionData(name=name)
    evt_new_session.fire(data)

    await backend.create(session, data)
    cookie.attach_to_response(response, session)

    return {
        "action":f"created session {session}",
        "id":data.node_id
    }

@router.post("/delete_session")
async def del_session(response: Response, session_id: UUID = Depends(cookie),session_data: SessionData = Depends(verifier)):
    evt_del_session.fire(session_data)
    await backend.delete(session_id)
    cookie.delete_from_response(response)
    del __cache[session_data.node_id]
    return {"action":"deleted session"}