#!/bin/bash

# Generate Self Signed SSL Certificates for the different environments

# Always refer to script location
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# For office-extern001
mkdir -p "$SCRIPT_DIR/office-extern001"
openssl req -x509 -newkey rsa:4096 -sha256 -days 3650 \
  -nodes -keyout "$SCRIPT_DIR/office-extern001/coordinator-controller.key" -out "$SCRIPT_DIR/office-extern001/coordinator-controller.crt" -subj "/CN=office-extern001" \
  -addext "subjectAltName=DNS:office-extern001,DNS:*.office-extern001,DNS:localhost,IP:127.0.0.1,IP:192.168.40.56,IP:10.42.0.1"

openssl req -x509 -newkey rsa:4096 -sha256 -days 3650 \
  -nodes -keyout "$SCRIPT_DIR/office-extern001/deviceclient-controller.key" -out "$SCRIPT_DIR/office-extern001/deviceclient-controller.crt" -subj "/CN=office-extern001" \
  -addext "subjectAltName=DNS:office-extern001,DNS:*.office-extern001,DNS:localhost,IP:127.0.0.1,IP:192.168.40.56,IP:10.42.0.1"

# For docker and localhost installations
mkdir -p "$SCRIPT_DIR/docker-local"
openssl req -x509 -newkey rsa:4096 -sha256 -days 3650 \
  -nodes -keyout "$SCRIPT_DIR/docker-local/coordinator-controller.key" -out "$SCRIPT_DIR/docker-local/coordinator-controller.crt" -subj "/CN=coordinator-controller" \
  -addext "subjectAltName=DNS:coordinator-controller,DNS:localhost,IP:127.0.0.1"

openssl req -x509 -newkey rsa:4096 -sha256 -days 3650 \
  -nodes -keyout "$SCRIPT_DIR/docker-local/deviceclient-controller.key" -out "$SCRIPT_DIR/docker-local/deviceclient-controller.crt" -subj "/CN=deviceclient-controller" \
  -addext "subjectAltName=DNS:deviceclient-controller,DNS:localhost,IP:127.0.0.1"
