from bot import ArmTankBot, Motor

class MockMotor(Motor):
    def __init__(self, port: int, speed: int) -> None:
        self.port: int = port
        self.defaultSpeed: int = speed
        self.intent: Motor.Intent = Motor.Intent.STOP
    
    def step(self):
        pass

class MockArmTankBot(ArmTankBot):
    def __init__(self) -> None:
        self._rightMotor: MockMotor = MockMotor(port=1, speed=-300)
        self.leftMotor: MockMotor   = MockMotor(port=2, speed=-300)
        self.armMotor: MockMotor    = MockMotor(port=3, speed=300)
        self.clawMotor: MockMotor   = MockMotor(port=4, speed=140)
        self._motors = [
            self._rightMotor,
            self.leftMotor,
            self.clawMotor,
            self.armMotor
        ]

    def action(func):
        def wrapper(*args, **kwargs):
            func(*args,**kwargs)
        return wrapper