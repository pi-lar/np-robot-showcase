from enum import Enum
from typing import List
from time import time as current_time, sleep

import asyncio

from megapi import *


def loop_in_thread(loop:asyncio.BaseEventLoop):
    asyncio.set_event_loop(loop)    
    loop.run_forever()
class Bot():
    def action(func):
        def wrapper(*args, **kwargs):
            bot:Bot = args[0]
            func(*args,**kwargs)
            #bot.step()
        return wrapper

    def __init__(self, loop:asyncio.BaseEventLoop=None) -> None:
        self._bot: MegaPi = MegaPi()
        self._motors: List[Motor] = []
        self._bot.start()
        self.goZero()
        self._loop : asyncio.BaseEventLoop = loop
        self._thread = None
        self.stop_thread = False
        if not self._loop:
            self._loop = asyncio.new_event_loop()            
            import threading
            self._thread = threading.Thread(target=loop_in_thread, args=(self._loop, ))
            self._thread.start()
        asyncio.run_coroutine_threadsafe(self._run(), self._loop)
    
    async def _run(self):
        while True:
            self.step()
            if self.stop_thread:
                break
            await asyncio.sleep(0.1)
            if self.stop_thread:
                break

    def step(self):
        for motor in self._motors:
            motor.step()
    
    def stop(self):
        for motor in self._motors:
            motor.stop()
        self.step()

    def __del__(self):
        if self._thread:
            self.stop_thread = True
        if self._bot:
            self._bot.close()

    def goZero(self):
        for motor in self._motors:
            motor.goZero()
class ArmTankBot(Bot):
    def __init__(self) -> None:
        super().__init__()
        self._rightMotor: Motor     = Motor(port=1, speed=-300, _bot=self._bot)
        self.leftMotor: Motor      = Motor(port=2, speed=-300, _bot=self._bot)
        self.armMotor: LimitMotor  = LimitMotor(port=3, speed=300, distance_per_second=2500, maxDistance=2500, _bot=self._bot)
        self.clawMotor: LimitMotor = LimitMotor(port=4, speed=140, distance_per_second=1000,  maxDistance=1000, _bot=self._bot)
        self._motors = [
            self._rightMotor,
            self.leftMotor,
            self.clawMotor,
            self.armMotor
        ]
        self.goZero()
    @Bot.action
    def armUp(self):
        self.armMotor.direction2()
    @Bot.action
    def armDown(self):
        self.armMotor.direction1()
    @Bot.action
    def armStop(self):
        self.armMotor.stop()
    @Bot.action        
    def clawOpen(self):
        self.clawMotor.direction1()
    @Bot.action
    def clawClose(self):
        self.clawMotor.direction2()
    @Bot.action
    def clawStop(self):
        self.clawMotor.stop()
    @Bot.action
    def turnLeft(self):
        self.leftMotor.direction2()
        self._rightMotor.direction2()
    @Bot.action
    def turnRight(self):
        self.leftMotor.direction1()
        self._rightMotor.direction1()
    @Bot.action
    def driveForward(self):
        self.leftMotor.direction2()
        self._rightMotor.direction1()
    @Bot.action
    def driveBackward(self):
        self.leftMotor.direction1()
        self._rightMotor.direction2()
    @Bot.action
    def driveStop(self):
        self.leftMotor.stop()
        self._rightMotor.stop()

class Motor(): 
    class Intent(Enum):
        STOP = 0
        DIRECTION1 = 1
        DIRECTION2 = -1

    def __init__(self, port: int, speed: int, _bot: MegaPi) -> None:
        self.port: int = port
        self._bot:MegaPi = _bot
        self.defaultSpeed: int = speed
        self.intent: Motor.Intent = Motor.Intent.STOP
        self._bot.encoderMotorRun(self.port, 0)

    def stop(self):
        self.intent = Motor.Intent.STOP

    def direction1(self):
        self.intent = Motor.Intent.DIRECTION1

    def direction2(self):
        self.intent = Motor.Intent.DIRECTION2

    def goZero(self):
        self.stop()
        self.step()
    
    def _getSpeed(self):
        speed = self.defaultSpeed
        if self.intent == Motor.Intent.DIRECTION2:
            speed *= -1
        elif self.intent == Motor.Intent.STOP:
            speed = 0
        return speed

    def step(self):
        self._bot.encoderMotorRun(
            self.port,
            self._getSpeed()
        )
class LimitMotor(Motor):
    def __init__(self, distance_per_second: int, maxDistance: int, **kwargs) -> None:
        super().__init__(**kwargs)
        self.distance_factor : float = distance_per_second / self.defaultSpeed
        self.maxDistance: int = maxDistance
        self.minDistance = 0
        self.currentDistance: int = maxDistance
        self.lastCommandExecuted: int = 0

    def step(self):
        now = current_time()

        time_distance = 0
        if self.lastCommandExecuted != 0:
            time_distance = now - self.lastCommandExecuted

        elapsed_distance = self.distance_factor * abs(self._getSpeed()) * time_distance

        if self.intent == Motor.Intent.DIRECTION1 :
            self.currentDistance -= elapsed_distance
        elif self.intent == Motor.Intent.DIRECTION2:
            self.currentDistance += elapsed_distance

        if self.intent == Motor.Intent.STOP:
            self.lastCommandExecuted = 0
        else:
            if  self.currentDistance < self.minDistance:
                self.currentDistance = self.minDistance
                self.stop()
                self.lastCommandExecuted = 0
            elif  self.currentDistance > self.maxDistance:
                self.currentDistance = self.maxDistance
                self.stop()
                self.lastCommandExecuted = 0
            else:
                self.lastCommandExecuted = now

        super().step()
        

    def goZero(self):
        self.direction2()
        super().step()
        sleep(3)
        self.currentDistance: int = self.maxDistance
        self.stop()
        super().step()

if __name__ == "__main__":
    bop = ArmTankBot()
    sleep(1)

    def loop():
        try:
            while True:
                print("loop")
                bop.step()
                sleep(1)
        except KeyboardInterrupt:
            bop.armStop()
            bop.clawStop()
            bop.driveStop()
            bop.step()
            print("stopped everything")

    #bop.armUp()
    #loop()


