from datetime import datetime, timedelta
from enum import IntEnum
import logging, os, sys, json, subprocess, io
from typing import Callable

from pydantic import validator,Field

from neuropil import neuropil, NeuropilNode, np_message, np_token, np_subject
from shared.models import DeviceStatus, Action, CoordinatorStatus, ActionPermissionMessage, ClaimDeviceMessage, BodyGroup
from mockbot import MockArmTankBot
from bot import ArmTankBot, Motor

logging.basicConfig(
    format = "%(levelname)s %(asctime)s - %(message)s",
    level = logging.DEBUG,
    handlers = [logging.StreamHandler(sys.stdout)]
)

def is_raspberrypi():
    try:
        with io.open('/sys/firmware/devicetree/base/model', 'r') as m:
            if 'raspberry pi' in m.read().lower(): return True
    except Exception: pass
    return False

bot = MockArmTankBot()
if is_raspberrypi():
    logging.info(f"setting up robot")
    bot = ArmTankBot()
    logging.info(f"done setting up robot")

    youtube_key = os.getenv("YOUTUBE_KEY", None)
    if youtube_key:
        logging.info(f"setting up livestream")
        #https://www.chefblogger.me/2021/09/02/youtube-livestream-mit-dem-raspberry-pi-machen/
        subprocess.run(f"raspivid -o - -t 0 -vf -hf -fps 30 -b 6000000 | ffmpeg -re -ar 44100 -ac 2 -acodec pcm_s16le -f s16le -ac 2 -i /dev/zero -f h264 -i - -vcodec copy -acodec aac -ab 128k -g 50 -strict experimental -f flv rtmp://x.rtmp.youtube.com/live2/{str(youtube_key)}")
    else:
        logging.info(f"no livestream")


class ActionRuntime(Action):
    subject:np_subject = Field(..., exclude=True)
    set_subject:np_subject = Field(..., exclude=True)
    class Config:
        arbitrary_types_allowed = True

def addActionRuntimeToDeviceStatus(device:DeviceStatus, action_id:str, group:BodyGroup=BodyGroup.OTHER):
    device.actions[action_id] = ActionRuntime(
        id          = action_id,
        client_id   = None,
        subject     = np_subject.generate(f"urn:io:neuropil:demo:device:{device.id}:{action_id}"),
        set_subject = np_subject.generate(f"urn:io:neuropil:demo:device:{device.id}:{action_id}:set"),
        bodyGroup       = group
    )

subject_device_status = "urn:io:neuropil:demo:device:status"
subject_device_status_np_subject = np_subject.generate(subject_device_status)

device:DeviceStatus = None

def cb_authenticate(node:NeuropilNode, token:np_token):
    # for the demo authenticate everybody
    logging.info(f"authenticated")
    return True

def cb_authorize(node:NeuropilNode, token:np_token):
    ret = True
    # everybody should receive the status of this device
    ret = str(subject_device_status_np_subject) == str(token.subject)
    if not ret:
        for action_id in device.actions:
            action:ActionRuntime = device.actions[action_id]
            if str(action.set_subject) == str(token.subject):
                # auto claim device
                if device.coordinator_id == None:
                    device.coordinator_id = str(token.issuer)
                    device.updateTime()
                    # send_status()
                    logging.info(f"Device is auto claimed by {device.coordinator_id}")
                ret = device.coordinator_id == str(token.issuer)
            if action.client_id != None:
                if str(action.subject) == str(token.subject):
                    ret = str(action.client_id) == str(token.issuer)
    logging.info(f"Authorize result {str(ret)} for issuer: {token.issuer} subject: {token.subject}")
    return ret

def toggleAction(deviceAction:ActionRuntime, botmove:Callable, message:np_message):
    global bot, device
    issuer = str(getattr(message, "from"))
    if str(deviceAction.client_id) != issuer:
        logging.info(f"Issuer id {issuer} does not match permitted id {deviceAction.client_id}")
        return
    if deviceAction.is_active:
        deviceAction.is_active = False
        if deviceAction.bodyGroup == BodyGroup.DRIVE:
            botmove = bot.driveStop
        elif deviceAction.bodyGroup == BodyGroup.CLAW:
            botmove = bot.clawStop
        elif deviceAction.bodyGroup == BodyGroup.ARM:
            botmove = bot.armStop
    else:
        for action in device.actions:
            if device.actions[action].bodyGroup == deviceAction.bodyGroup:
                device.actions[action].is_active = False
        deviceAction.is_active = True
    device.updateTime()
    logging.info(f"Move {deviceAction.id}")
    if bot:
        botmove()
    send_status()

def left(node:NeuropilNode, message:np_message):
    global bot, device
    toggleAction(device.actions["left"], bot.turnLeft, message)
def right(node:NeuropilNode, message:np_message):
    global bot, device
    toggleAction(device.actions["right"], bot.turnRight, message)
def forward(node:NeuropilNode, message:np_message):
    global bot, device
    toggleAction(device.actions["forward"], bot.driveForward, message)
def reverse(node:NeuropilNode, message:np_message):    
    global bot, device
    toggleAction(device.actions["reverse"], bot.driveBackward, message)
def arm_up(node:NeuropilNode, message:np_message):
    global bot, device
    toggleAction(device.actions["arm_up"], bot.armUp, message)
def arm_down(node:NeuropilNode, message:np_message):
    global bot, device
    toggleAction(device.actions["arm_down"], bot.armDown, message)
def claw_open(node:NeuropilNode, message:np_message):
    global bot, device
    toggleAction(device.actions["claw_open"], bot.clawOpen, message)
def claw_close(node:NeuropilNode, message:np_message):
    global bot, device
    toggleAction(device.actions["claw_close"], bot.clawClose, message)

def claim(node:NeuropilNode, message:np_message):
    logging.info(f"Device is claimed by {device.coordinator_id}")

def set_permission(action:str, message:np_message):
    msg = ActionPermissionMessage.parse_raw(message.raw().decode("utf-8"))
    old = device.actions[action].client_id
    if not msg.client_id or len(msg.client_id) != 64:
        device.actions[action].client_id = None
    else:
        device.actions[action].client_id = msg.client_id

    if old != device.actions[action].client_id:
        device.updateTime()
        logging.info(f"Set permission for action {action} to {msg.client_id}")

def set_left(node:NeuropilNode, message:np_message):
    set_permission("left", message)
    send_status()
def set_right(node:NeuropilNode, message:np_message):
    set_permission("right", message)
    send_status()
def set_forward(node:NeuropilNode, message:np_message):
    set_permission("forward", message)
    send_status()
def set_reverse(node:NeuropilNode, message:np_message):
    set_permission("reverse", message)
    send_status()
def set_arm_up(node:NeuropilNode, message:np_message):
    set_permission("arm_up", message)
    send_status()
def set_arm_down(node:NeuropilNode, message:np_message):
    set_permission("arm_down", message)
    send_status()
def set_claw_open(node:NeuropilNode, message:np_message):
    set_permission("claw_open", message)
    send_status()
def set_claw_close(node:NeuropilNode, message:np_message):
    set_permission("claw_close", message)
    send_status()

def set_claim(node:NeuropilNode, message:np_message):
    msg = ClaimDeviceMessage.parse_raw(message.raw().decode("utf-8"))
    device.coordinator_id = msg.client_id
    logging.info(f"Device is claimed by {device.coordinator_id}")
    send_status()

node = NeuropilNode(9000,
    host="0.0.0.0",
    proto="pas4",
    auto_run=False, 
    log_file="neuropil_robot.log",
    log_level=0,
    n_threads=0
)

node_id=node.get_address()[:64]

device = DeviceStatus(
    id=node_id,
    type="claw-robot",
    availableBodyGroups=[BodyGroup.DRIVE, BodyGroup.ARM, BodyGroup.CLAW]
    )
addActionRuntimeToDeviceStatus(device,"left",BodyGroup.DRIVE)
addActionRuntimeToDeviceStatus(device,"right",BodyGroup.DRIVE)
addActionRuntimeToDeviceStatus(device,"forward",BodyGroup.DRIVE)
addActionRuntimeToDeviceStatus(device,"reverse",BodyGroup.DRIVE)
addActionRuntimeToDeviceStatus(device,"arm_up",BodyGroup.ARM)
addActionRuntimeToDeviceStatus(device,"arm_down",BodyGroup.ARM)
addActionRuntimeToDeviceStatus(device,"claw_open",BodyGroup.CLAW)
addActionRuntimeToDeviceStatus(device,"claw_close",BodyGroup.CLAW)
addActionRuntimeToDeviceStatus(device,"claim")

node.set_authenticate_cb(cb_authenticate)
node.set_authorize_cb(cb_authorize)
    
mxp = node.get_mx_properties(subject_device_status)
mxp.ackmode = neuropil.NP_MX_ACK_NONE
mxp.role = neuropil.NP_MX_PROVIDER
mxp.intent_ttl = 3600
mxp.intent_update_after = 60
mxp.max_retry = 3
mxp.apply()

for action_id in device.actions.keys():
    action:ActionRuntime = device.actions[action_id]
    mxp = node.get_mx_properties(action.subject)
    mxp.ackmode = neuropil.NP_MX_ACK_DESTINATION
    mxp.role = neuropil.NP_MX_CONSUMER
    mxp.intent_ttl = 3600
    mxp.intent_update_after = 60
    mxp.max_retry = 3
    mxp.apply()
    node.set_receive_cb(action.subject, locals()[action_id])

    mxp = node.get_mx_properties(action.set_subject)
    mxp.ackmode = neuropil.NP_MX_ACK_DESTINATION
    mxp.role = neuropil.NP_MX_CONSUMER
    mxp.intent_ttl = 3600
    mxp.intent_update_after = 60
    mxp.max_retry = 3
    mxp.apply()    
    node.set_receive_cb(action.set_subject, locals()["set_" + action_id])

node.run(0)
logging.info("Running    device neuropil node: " + node.get_address())

bootstrapper:str = os.getenv("NP_BOOTSTRAPPER")
node.join(bootstrapper)
logging.info(f'Joining {bootstrapper}')

def send_status():
    global device, node, bot

    if bot:
        device.actions["left"].is_active = bot.leftMotor.intent == Motor.Intent.DIRECTION2 and bot._rightMotor.intent == Motor.Intent.DIRECTION2
        device.actions["right"].is_active = bot.leftMotor.intent == Motor.Intent.DIRECTION1 and bot._rightMotor.intent == Motor.Intent.DIRECTION1
        device.actions["forward"].is_active = bot.leftMotor.intent == Motor.Intent.DIRECTION2 and bot._rightMotor.intent == Motor.Intent.DIRECTION1
        device.actions["reverse"].is_active = bot.leftMotor.intent == Motor.Intent.DIRECTION1 and bot._rightMotor.intent == Motor.Intent.DIRECTION2
        device.actions["arm_up"].is_active = bot.armMotor.intent == Motor.Intent.DIRECTION2
        device.actions["arm_down"].is_active = bot.armMotor.intent == Motor.Intent.DIRECTION1
        device.actions["claw_open"].is_active = bot.clawMotor.intent == Motor.Intent.DIRECTION1
        device.actions["claw_close"].is_active = bot.clawMotor.intent == Motor.Intent.DIRECTION2
        device.updateTime()
    node.send(subject_device_status, device.model_dump_json(indent=0))
    #logging.info(f'Send status')

currentTime = datetime.now()
maxTime = timedelta(seconds=5)
try:
    while node.run(0.1) == neuropil.np_ok:
        if datetime.now() - currentTime >= maxTime:
            currentTime = datetime.now()
            send_status()
        if not node.has_joined():
            logging.info(f'Rejoining {bootstrapper}')
            node.join(bootstrapper)
finally:
    node.shutdown(False)

logging.info("Device shutdown")