FROM registry.gitlab.com/pi-lar/neuropil/python-enviroment:neuropil_0.12.2
ARG ROOTDIR=robot

ENV NP_BOOTSTRAPPER <HAS TO BE SET>

RUN mkdir /app

WORKDIR /app

COPY $ROOTDIR/requirements.txt requirements.txt

RUN pip3 install -r requirements.txt

COPY $ROOTDIR/src/* ./
COPY shared_backend/* ./shared/

EXPOSE 9000

ENTRYPOINT [ "python3","main.py"]