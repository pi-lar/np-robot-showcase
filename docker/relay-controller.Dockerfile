FROM registry.gitlab.com/pi-lar/neuropil/python-enviroment:neuropil_0.12.2
ARG ROOTDIR=relay-controller

ENV HOST=relay-controller

RUN mkdir -p /app

WORKDIR /app

COPY $ROOTDIR/requirements.txt requirements.txt

RUN pip3 install -r requirements.txt

EXPOSE 9999

COPY $ROOTDIR /app/

ENTRYPOINT [ "python3","/app/backend/main.py"]