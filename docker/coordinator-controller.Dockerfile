FROM node:16 as base
ARG ROOTDIR=coordinator-controller

COPY $ROOTDIR/frontend/package.json ./

RUN npm install

COPY $ROOTDIR/frontend/ ./

RUN npm run build

FROM registry.gitlab.com/pi-lar/neuropil/python-enviroment:neuropil_0.12.2
ARG ROOTDIR=coordinator-controller

ENV NP_BOOTSTRAPPER <HAS TO BE SET>

RUN mkdir -p /app

WORKDIR /app

COPY $ROOTDIR/requirements.txt requirements.txt

RUN pip3 install -r requirements.txt

COPY --from=base dist dist

COPY $ROOTDIR/backend/ ./
COPY shared_backend/ ./app/shared/

EXPOSE 9000

ENTRYPOINT [ "uvicorn","main:app", "--proxy-headers", "--host", "0.0.0.0", "--port", "9000","--log-level","warning", \
"--ssl-keyfile","/app/ssl_cert/coordinator-controller.key","--ssl-certfile","/app/ssl_cert/coordinator-controller.crt"]