# Neuropil Robot Showcase

This repository does hold all applications nesseccary to showcase the [neuropil](https://neuropil.org) messaging layer in conjunction with a reactive web frontend as well as an IoT device which can be controlled by different participants of the showcase.

All communication between the applications is done via [neuropil](https://neuropil.org). 

All applications can therefore reside in different, securly seperated, networks and connect through predefined instances of the `controller` application

If you do have further interest in the [neuropil](https://neuropil.org) messaging layer please get in touch
Email: info@pi-lar.net
Website: https://www.neuropil.io

## Technical details

If you want to try it out yourself you can clone this repository, generate self signed ssl certificates and run the docker compose command.
  - Install [docker](https://docs.docker.com/get-docker/)
  - Install [docker compose](https://docs.docker.com/compose/install/)
  - Install [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
```
$ git clone https://gitlab.com/pi-lar/np-robot-showcase.git
$ cd np-robot-showcase
$ ./ssl_certs/generate.sh
$ docker-compose up --build
```
After the building is done you can access the deviceclient controller on http://localhost:8000, the coordinator controller on http://localhost:9000 and see the robot result in the terminal log (or on the robot if you deployed it there).

### Applications

#### Controller
The Controller is a basic [neuropil](https://neuropil.org) node which only serves as an entry point for the neuropil overlay network.
It has to be accasable by every other application of this showcase.
##### Technology:
 - Neuropil Node example application
 - Docker

#### Coordinator Controller
The coordinator controller decides which deviceclient controller has control over which device action.

##### Technology:
 - Neuropil
 - Vue.js (JavaScript)
 - FastAPI (python)

#### Deviceclient Controller
The deviceclient controller can control certain device actions if it is allowed to do so by the coordinator controller.

##### Technology:
 - Neuropil
 - Vue.js (JavaScript)
 - FastAPI (python)
   
#### Robot
The robot receives action commands from the deviceclient controllers and, if they match the permission given by the coordinator controller, executes them.
Implemented actions:
left, right, forward, reverse, arm_up, arm_down, claw_open, claw_close, claim

##### Technology:
 - Neuropil
 - python
 - Makeblock Ultimate 2.0 (optional)

### Subject overview:
  - Deviceclient Controller
    - :arrow_left: `urn:io:neuropil:demo:device:status`
    - :arrow_right: `urn:io:neuropil:demo:deviceclient:status`
    - :arrow_right: `urn:io:neuropil:demo:deviceclient:device_request`
    - :arrow_right: `urn:io:neuropil:demo:device:<device neuropil node id>:<action>`
  - Coordinator Controller
    - :arrow_left::arrow_right: `urn:io:neuropil:demo:coordinator:<coordinator neuropil node id>:device:requst`
    - :arrow_left::arrow_right: `urn:io:neuropil:demo:coordinator:status`
    - :arrow_left: `urn:io:neuropil:demo:deviceclient:status`
    - :arrow_left: `urn:io:neuropil:demo:deviceclient:device_request`
    - :arrow_left: `urn:io:neuropil:demo:device:status`
    - :arrow_right: `urn:io:neuropil:demo:device:<device neuropil node id>:<action>:set`
  - Robot
    - :arrow_right: `urn:io:neuropil:demo:device:status`
    - :arrow_left: `urn:io:neuropil:demo:device:<device neuropil node id>:<action>`
    - :arrow_left: `urn:io:neuropil:demo:device:<device neuropil node id>:<action>:set`