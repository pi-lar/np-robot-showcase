import os, sys, logging
from neuropil import neuropil, NeuropilNode, np_token


logging.basicConfig(
    format = "%(levelname)s %(asctime)s - %(message)s",
    level = logging.DEBUG,
    handlers = [logging.StreamHandler(sys.stdout)]
)


def cb_authenticate(node:NeuropilNode, token:np_token):
    logging.info(f"authenticated {token.get_fingerprint()} / {token.subject}")
    return True

def cb_authorize(node:NeuropilNode, token:np_token):
    logging.info(f"authorized {token.get_fingerprint()} / {token.subject}")
    return False

host = str(os.getenv("HOST"))
port = int(os.getenv("PORT","9999"))
proto = str(os.getenv("PROTO","udp4"))
logging.info(f"using HOST={host} PORT={port} PROTO={proto}")

node = NeuropilNode(port, 
    host=host,
    proto=proto,
    auto_run=False,
    log_file="neuropil_relay.log",
    leafset_size=99
)

node.set_authenticate_cb(cb_authenticate)
node.set_authorize_cb(cb_authorize)

logging.info(f"Running     relay neuropil node: {node.get_address()}")
try:
    status = neuropil.np_ok
    while  status == neuropil.np_ok:
        status = node.run(1)
    logging.info(f"node is not in running status: {status}")
    node.shutdown(False)
except Exception:
    logging.error(f"Exception registertd. try to shut down")
    node.shutdown(True)
    raise