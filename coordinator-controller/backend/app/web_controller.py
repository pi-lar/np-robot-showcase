import logging
from typing import List, Set, Dict, Optional, Union

import asyncio
from socketio import AsyncServer
from fastapi import HTTPException, FastAPI, APIRouter, Response, Depends

from .models.Runtime import *
from .shared.session import router as session_router, cookie, verifier, evt_new_session, evt_del_session, SessionData
from .shared.models import BodyGroup

logger = logging.getLogger("app")
router = APIRouter()
_runtime:Runtime = None

def init(websocket:AsyncServer, http:FastAPI, loop:asyncio.BaseEventLoop = asyncio.get_running_loop()):
    global _runtime, evt_new_session
    _runtime = Runtime(websocket, http, loop)
    _init_websockets(websocket)
    http.include_router(router)
    http.include_router(session_router)
    evt_new_session += _runtime.cb_new_session
    loop.create_task(sendStatusMsgs())

async def sendStatusMsgs():
    global _runtime
    while True:
        for session in _runtime.cache.sessions:
            session.node.sendStatus()
            await asyncio.sleep(1)
        await asyncio.sleep(3)
        
@router.patch("/set_device/{client_id}/", dependencies=[Depends(cookie)])
@router.patch("/set_device/{client_id}/{device_id}", dependencies=[Depends(cookie)])
async def _set_device_for_client(response: Response, client_id:str, device_id:Optional[str]=None, session_data: SessionData = Depends(verifier)):
    global _runtime
    ret = {}
    if client_id not in _runtime.cache.clients: raise HTTPException(status_code=404, detail=f"Client {client_id} not found")
    client = _runtime.cache.clients[client_id]        
    if device_id:
        if device_id not in _runtime.cache.devices: raise HTTPException(status_code=404, detail=f"Client {device_id} not found")
        device = _runtime.cache.devices[device_id]
        device_id = device.id

    client.device_id = device_id

    await _runtime.websocket.emit('update_client', client.dict())
    return client.dict()
        
@router.post("/request/device/ownership/{device_id}", dependencies=[Depends(cookie)])
async def _request_device_ownership(response: Response, device_id:str,session_data: SessionData = Depends(verifier)):
    global _runtime
    if device_id not in _runtime.cache.devices: raise HTTPException(status_code=404, detail=f"Device {device_id} not found")
    device = _runtime.cache.devices[device_id]
    
    node = _runtime.cache.findSessionByClientId(session_data.node_id).node
    logger.info(f"{node.id} initiates device transfer from {device.coordinator_id} to {node.id} device: {device.id}")
    node.requestDeviceOwnership(device)
    await asyncio.sleep(0.5)
    node.requestDeviceOwnership(device)

@router.post("/set/device/ownership/{device_id}/{owner_id}", dependencies=[Depends(cookie)])
async def _request_device_ownership(response: Response, device_id:str,owner_id:str,session_data: SessionData = Depends(verifier)):
    global _runtime
    if device_id not in _runtime.cache.devices: raise HTTPException(status_code=404, detail=f"Device {device_id} not found")
    device = _runtime.cache.devices[device_id]

    node = _runtime.cache.findSessionByClientId(session_data.node_id).node
    node.setDeviceOwnership(device, owner_id)

@router.patch("/set_device_permission/{client_id}/{device_id}/{bodyGroup}", dependencies=[Depends(cookie)])
async def _set_device_permission_for_client(response: Response, client_id:str, device_id:str, bodyGroup:BodyGroup,session_data: SessionData = Depends(verifier)):
    global _runtime
    if client_id not in _runtime.cache.clients: raise HTTPException(status_code=404, detail=f"Client {client_id} not found")
    client = _runtime.cache.clients[client_id]
    if device_id not in _runtime.cache.devices: raise HTTPException(status_code=404, detail=f"Device {device_id} not found")
    device = _runtime.cache.devices[device_id]
    # if action_id not in device.actions.keys(): raise HTTPException(status_code=404, detail=f"Action {action_id} not found on device")
    
    node = _runtime.cache.findSessionByClientId(session_data.node_id).node
    for action in device.actions.values():
        if action.bodyGroup == bodyGroup:
            if action.client_id == client.id:
                logging.info(f"Revoking permission for action {action.id} of {device.id} to {client.id}")
                node.setActionPermission(device, action, None)
            else:
                logging.info(f"Setting permission for action {action.id} of {device.id} to {client.id}")
                node.setActionPermission(device, action, client)

    await _runtime.websocket.emit('update_client', client.dict())
    return client.dict()

@router.post("/set_intent/{device_id}", dependencies=[Depends(cookie)])
async def _set_intent(response: Response, device_id:str,session_data: SessionData = Depends(verifier)):
    global _runtime
    node = _runtime.cache.findSessionByClientId(session_data.node_id).node
    if device_id != "null":
        if device_id not in _runtime.cache.devices: raise HTTPException(status_code=404, detail=f"Device {device_id} not found")

        logging.info(f"Set Intent to {device_id}")
        node.coordinatorStatus.intendedDeviceId = device_id
    else:
        logging.info(f"Set no Intent")
        node.coordinatorStatus.intendedDeviceId = None
    node.sendStatus()


@router.get("/clearcache")
async def _clearcache():  
    global _runtime  
    _runtime.clearCache()

@router.get("/status", dependencies=[Depends(cookie)])
async def _status(response: Response, session_data: SessionData = Depends(verifier)):
    global _runtime
    node = _runtime.cache.findSessionByClientId(session_data.node_id).node

    node.join()
    ret = {
        "node_id": node.id,
        "node_status": node.get_status(),
        **_runtime.cache.dict(),
        "name":session_data.name
    }
    await _runtime.websocket.emit('status'+session_data.node_id, ret)
    return ret

def _init_websockets(websocket):
    @websocket.event
    async def _connect(sid, environ):
        global _runtime
        logging.info('connect: '+ sid )
        await _status()
        
    @websocket.event
    def _disconnect(sid):
        global _runtime
        logging.info('disconnect: '+ sid)        