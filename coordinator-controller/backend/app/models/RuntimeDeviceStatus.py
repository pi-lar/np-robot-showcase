import time
from ..shared.models import DeviceStatus

class RuntimeDeviceStatus(DeviceStatus):
    processed_at:int = time.time()
