import logging, os

from socketio import AsyncServer
from fastapi import FastAPI
import asyncio

from ..shared.models import DeviceStatus, ClientStatus, CoordinatorStatus, RequestDeviceMessage,ClaimDeviceMessage
from ..shared.session import SessionData
from ..node import CustomNeuropilNode
from .Cache import *

logger = logging.getLogger("app")

class Runtime():
    def __init__(self,websocket:AsyncServer, http:FastAPI, loop:asyncio.BaseEventLoop):

        self.websocket = websocket
        self.http = http
        self.loop = loop
        self.cache = Cache()

    def clearCache(self):
        self.cache = Cache()

    def _handle_device_status(self, node:CustomNeuropilNode, device:DeviceStatus):
        is_new_device = not device.id in self.cache.devices
        logger.info(f"Got device status {device.id}")
        if is_new_device:
            logger.info(f"Detected new device {device.id}")
            node.registerDevice(device)
            
        if is_new_device or self.cache.devices[device.id].time_of_alteration < device.time_of_alteration:
            logger.info(f"Got device update {device.id}")
            self.cache.devices[device.id] = device
            self.loop.create_task(self.websocket.emit('update_device', self.cache.devices[device.id].dict()))

    def _handle_client_status(self,node:CustomNeuropilNode,client:ClientStatus):
        is_new_client =  not client.id in self.cache.clients
        old_client_data = ""
        if is_new_client:
            logger.info(f"Detected new client {client.id}")
            self.cache.clients[client.id] = RuntimeClientStatus(**client.dict())
        else:
            old_client_data = self.cache.clients[client.id].json()
            self.cache.clients[client.id] = self.cache.clients[client.id].copy(update=client.dict())
        if is_new_client or old_client_data != self.cache.clients[client.id].json():
            self.loop.create_task(self.websocket.emit('update_client', client.dict()))

    def _handle_request_device(self,node:CustomNeuropilNode,msg:RequestDeviceMessage):
        logger.info(f"client {msg.client_id} requests device {msg.bodyGroup}")
        self.cache.clients[msg.client_id].addRequest(msg)
        self.loop.create_task(self.websocket.emit('device_request', msg.dict()))

    def _handle_request_device_ownership(self,node:CustomNeuropilNode,msg:ClaimDeviceMessage):
        logger.info(f"client {msg.client_id} requests device ownership of {msg.device_id}")
        self.loop.create_task(self.websocket.emit('device_request_ownership'+node.id, msg.dict()))

    def _handle_coordinator_status(self,node:CustomNeuropilNode,coordinator:CoordinatorStatus):
        is_new_coordinator =  not coordinator.id in self.cache.coordinators
        old_coordinator_data = ""
        if is_new_coordinator:
            logger.info(f"Detected new coordinator {coordinator.id}")
            self.cache.coordinators[coordinator.id] = CoordinatorStatus(**coordinator.dict())
        else:
            old_coordinator_data = self.cache.coordinators[coordinator.id].json()
            self.cache.coordinators[coordinator.id] = self.cache.coordinators[coordinator.id].copy(update=coordinator.dict())
        if is_new_coordinator or old_coordinator_data != self.cache.coordinators[coordinator.id].json():
            self.loop.create_task(self.websocket.emit('update_coordinator', coordinator.dict()))
    
    def cb_new_session(self, data:SessionData):
        node = CustomNeuropilNode(
            port = 9010,
            proto = "pas4",
            host="0.0.0.0",
            bootstrapper = os.getenv("NP_BOOTSTRAPPER")
        )
        node.evt_device_status += self._handle_device_status
        node.evt_client_status += self._handle_client_status
        node.evt_coordinator_status += self._handle_coordinator_status
        node.evt_request_device += self._handle_request_device
        node.evt_request_device_ownership += self._handle_request_device_ownership
        data.node_id = node.id
        node.coordinatorStatus = CoordinatorStatus(id=node.id, name=data.name)
        for session in  self.cache.sessions:
            session.node.registerOwner(node.id)
            node.registerOwner(session.node.id)
        self.cache.sessions.append(Session(sessionData=data,node=node))

