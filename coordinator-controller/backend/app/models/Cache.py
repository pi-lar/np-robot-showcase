import logging
from typing import List, Dict, Union

from pydantic import BaseModel,Field

from ..shared.models import DeviceStatus
from .Session import *
from .RuntimeClientStatus import *

logger = logging.getLogger("app")

class Cache(BaseModel):
    sessions:List[Session] = Field([], exclude=True)

    devices:Dict[(str,DeviceStatus)] = {}
    clients:Dict[(str,RuntimeClientStatus)] = {}
    coordinators:Dict[(str,RuntimeClientStatus)] = {}

    def findSessionByClientId(self,node_id:str) -> Union[Session,None]:
        for session in self.sessions:
            if session.sessionData.node_id == node_id:
                return session
        return None

    def __del__(self):
        for session in self.sessions:
            session.node.shutdown(False)
