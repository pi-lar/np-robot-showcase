import logging

from pydantic import BaseModel

from ..shared.session import SessionData
from ..node import CustomNeuropilNode

logger = logging.getLogger("app")

class Session(BaseModel):
    sessionData:SessionData
    node:CustomNeuropilNode
    class Config:
        arbitrary_types_allowed = True
