import logging
from typing import List, Optional

from ..shared.models import ClientStatus, RequestDeviceMessage

logger = logging.getLogger("app")

class RuntimeClientStatus(ClientStatus):
    device_id:Optional[str]             = None
    requests:List[RequestDeviceMessage] = []
    limitOfCachedRequests:int           = 10

    def addRequest(self, request:RequestDeviceMessage):
        if len(self.requests) >= self.limitOfCachedRequests:
            self.requests.pop(0)
        self.requests.append(request)
