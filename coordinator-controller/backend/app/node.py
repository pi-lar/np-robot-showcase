from typing import List, Set
import logging, json

from .shared.event import EventHook
from .shared.models import Action, CoordinatorStatus, ClientStatus, RequestDeviceMessage, ActionPermissionMessage, ClaimDeviceMessage
from .models.RuntimeDeviceStatus import *

from neuropil import neuropil, NeuropilNode, np_message, np_token, np_subject

subject_client_status = "urn:io:neuropil:demo:deviceclient:status"
subject_request_device  = "urn:io:neuropil:demo:deviceclient:device_request"
subject_device_status   = "urn:io:neuropil:demo:device:status"
subject_coordinator_status   = "urn:io:neuropil:demo:coordinator:status"

logger = logging.getLogger("app")

class CustomNeuropilNode(NeuropilNode):

    def __init__(self, bootstrapper:str, **kwargs):
        super().__init__(log_level=neuropil.LOG_NONE, **kwargs)
        self.id = self.get_address()[:64]
        self.set_authenticate_cb(self._cb_authenticate)
        self.set_authorize_cb(self._cb_authorize)

        mxp = self.get_mx_properties(subject_client_status)
        mxp.ackmode = neuropil.NP_MX_ACK_NONE
        mxp.role = neuropil.NP_MX_CONSUMER
        mxp.intent_ttl = 3600
        mxp.intent_update_after = 60
        mxp.max_retry = 3
        mxp.apply()
        self.set_receive_cb(subject_client_status, self._cb_client_status)
        self.evt_client_status = EventHook()

        mxp = self.get_mx_properties(subject_device_status)
        mxp.ackmode = neuropil.NP_MX_ACK_NONE
        mxp.role = neuropil.NP_MX_CONSUMER
        mxp.intent_ttl = 3600
        mxp.intent_update_after = 60
        mxp.max_retry = 3
        mxp.apply()
        self.set_receive_cb(subject_device_status, self._cb_device_status)
        self.evt_device_status = EventHook()

        mxp = self.get_mx_properties(subject_coordinator_status)
        mxp.ackmode = neuropil.NP_MX_ACK_NONE
        mxp.role = neuropil.NP_MX_PROVIDER
        mxp.intent_ttl = 3600
        mxp.intent_update_after = 60
        mxp.max_retry = 3
        mxp.apply()

        mxp = self.get_mx_properties(subject_coordinator_status)
        mxp.ackmode = neuropil.NP_MX_ACK_NONE
        mxp.role = neuropil.NP_MX_CONSUMER
        mxp.intent_ttl = 3600
        mxp.intent_update_after = 60
        mxp.max_retry = 3
        mxp.apply()
        self.set_receive_cb(subject_coordinator_status, self._cb_coordinator_status)
        self.evt_coordinator_status = EventHook()

        mxp = self.get_mx_properties(subject_request_device)
        mxp.ackmode = neuropil.NP_MX_ACK_DESTINATION
        mxp.role = neuropil.NP_MX_CONSUMER
        mxp.intent_ttl = 3600
        mxp.intent_update_after = 60
        mxp.max_retry = 3
        mxp.apply()
        self.set_receive_cb(subject_request_device, self._cb_request_device)
        self.evt_request_device = EventHook()

        mxp = self.get_mx_properties(f"urn:io:neuropil:demo:coordinator:{self.id}:device:requst")
        mxp.ackmode = neuropil.NP_MX_ACK_DESTINATION
        mxp.role = neuropil.NP_MX_CONSUMER
        mxp.intent_ttl = 3600
        mxp.intent_update_after = 60
        mxp.max_retry = 3
        mxp.apply()
        self.set_receive_cb(f"urn:io:neuropil:demo:coordinator:{self.id}:device:requst", self._cb_request_device_ownership)
        self.evt_request_device_ownership = EventHook()

        self.bootstrapper = bootstrapper
        logger.info(f"Running coordinator neuropil node: {self.get_address()}")#[:64]
        self.join()
        self.run(0)

        self.coordinatorStatus:CoordinatorStatus

    def join(self):
        if not self.has_joined():
            super().join(self.bootstrapper)
            logger.info(f"Joining {self.bootstrapper}")

    def _cb_authenticate(self, node:NeuropilNode, token:np_token):
        logger.info("Authenticate " + token.subject)
        return True

    def _cb_authorize(self, node:NeuropilNode, token:np_token):
        logger.info("Authorize " + token.subject)
        return True

    def registerDevice(self, device:RuntimeDeviceStatus):
        for action_id in device.actions:
            mxp = self.get_mx_properties(np_subject.generate(f"urn:io:neuropil:demo:device:{device.id}:{action_id}:set"))
            mxp.ackmode = neuropil.NP_MX_ACK_DESTINATION
            mxp.role = neuropil.NP_MX_PROVIDER
            mxp.intent_ttl = 120
            mxp.intent_update_after = 60
            mxp.max_retry = 3
            mxp.apply()
    
    def registerOwner(self, coordinator_id):
        mxp = self.get_mx_properties(f"urn:io:neuropil:demo:coordinator:{coordinator_id}:device:requst")
        mxp.ackmode = neuropil.NP_MX_ACK_DESTINATION
        mxp.role = neuropil.NP_MX_PROVIDER
        mxp.intent_ttl = 120
        mxp.intent_update_after = 60
        mxp.max_retry = 3
        mxp.apply()

    def setActionPermission(self, device:RuntimeDeviceStatus, action:Action,client:ClientStatus):
        msg = ActionPermissionMessage(client_id="")
        if client:
            msg.client_id = client.id

        self.send(f"urn:io:neuropil:demo:device:{device.id}:{action.id}:set", msg.json())

    def requestDeviceOwnership(self, device:RuntimeDeviceStatus):
        msg = ClaimDeviceMessage(client_id=self.id,device_id=device.id)
        self.send(f"urn:io:neuropil:demo:coordinator:{device.coordinator_id}:device:requst", msg.json())

    def setDeviceOwnership(self, device:RuntimeDeviceStatus, owner_id:str):
        msg = ClaimDeviceMessage(client_id=owner_id,device_id=device.id)
        self.send(f"urn:io:neuropil:demo:device:{device.id}:claim:set", msg.json())

    def sendStatus(self):
        msg = self.coordinatorStatus
        self.send(subject_coordinator_status, msg.json())

    def _cb_device_status(self, node:NeuropilNode, message:np_message):
        self.evt_device_status.fire(self, RuntimeDeviceStatus.parse_raw(message.raw().decode("utf-8")))

    def _cb_client_status(self, node:NeuropilNode, message:np_message):
        client = ClientStatus.parse_raw(message.raw().decode("utf-8"))
        self.evt_client_status.fire(self,client)

    def _cb_coordinator_status(self, node:NeuropilNode, message:np_message):
        coordinator = CoordinatorStatus.parse_raw(message.raw().decode("utf-8"))
        self.evt_coordinator_status.fire(self,coordinator)

    def _cb_request_device(self, node:NeuropilNode, message:np_message):
        msg = RequestDeviceMessage.parse_raw(message.raw().decode("utf-8"))
        self.evt_request_device.fire(self,msg)

    def _cb_request_device_ownership(self, node:NeuropilNode, message:np_message):
        msg = ClaimDeviceMessage.parse_raw(message.raw().decode("utf-8"))
        logger.info(f"{msg.client_id} requests on {node.id} device {msg.device_id}")
        self.evt_request_device_ownership.fire(self,msg)