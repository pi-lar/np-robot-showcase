# Software architecture

## Class diagram of Robot

```plantuml
@startuml Class Diagramm Robot
namespace Robot {
  class Bot {
    _bot: MegaPi
    _motors: List[Motor]
    ' _loop: asyncio.BaseEventLoop
    ' _thread
    ' stop_thread
  	_run() -> None
    step() -> None
    stop() -> None
    goZero() -> None
    }
  class ArmTankBot {
    _rightMotor: Motor
    leftMotor: Motor
    armMotor: LimitMotor
    clawMotor: LimitMotor
    armUp() -> None
    armDown() -> None
    armStop() -> None
    clawOpen() -> None
    clawClose() -> None
    clawStop() -> None
    turnLeft() -> None
    turnRight() -> None
    driveForward() -> None
    driveBackward() -> None
    driveStop() -> None
    }
  class Motor {
    _bot: MegaPi
    port: int
    defaultSpeed: int
    intent: Intent
    stop() -> None
    direction1() -> None
    direction2() -> None
    step() -> None
    goZero() -> None
    _getSpeed() -> None
    }
  class LimitMotor {
    distance_factor: float
    maxDistance: int
    minDistance: int
    currentDistance: int
    lastCommandExecuted: int
    step() -> None
    goZero() -> None
    }
  enum Intent {
    STOP
    DIRECTION1
    DIRECTION2
    }
  class main {
    __device: DeviceStatus__
    __node: NeuropilNode__
    __bot: ArmTankBot__
    is_raspberrypi() -> bool
    addActionRuntimeToDeviceStatus(device, action_id, group) -> None
  	cb_authenticate(node, token) -> bool
    cb_authorize(node, token) -> bool
    _move_device(action) -> None
    toggleAction(deviceAction, botmove) -> None
    left(node, message) -> None
    right(node, message) -> None
    forward(node, message) -> None
    reverse(node, message) -> None
    arm_up(node, message) -> None
    arm_down(node, message) -> None
    claw_open(node, message) -> None
    claw_close(node, message) -> None
    claim(node, message) -> None
    set_permission(action, message) -> None
    set_left(node, message) -> None
    set_right(node, message) -> None
    set_forward(node, message) -> None
    set_reverse(node, message) -> None
    set_arm_up(node, message) -> None
    set_arm_down(node, message) -> None
    set_claw_open(node, message) -> None
    set_claw_close(node, message) -> None
    set_claim(node, message) -> None
    send_status() -> None
    }
  class ActionRuntime {
    subject: np_subject
    set_subject: np_subject
    }  
  Bot <|-- ArmTankBot
  ArmTankBot "1" *-- "2" Motor
  ArmTankBot "1" *-- "2" LimitMotor
  Motor <|- LimitMotor
  main "1" *- "1" ArmTankBot
  Motor "1" o-- "1" Intent
  LimitMotor "1" o- "1" Intent
  Robot.main <.. ActionRuntime
  }
namespace Shared {
  enum BodyGroup {
    OTHER
    DRIVE
    CLAW
    ARM
    Camera
    }
  class Status {
    time_of_alteration: int
    created_at: int
    }
  class Action {
    id: str
    bodyGroup: BodyGroup
    client_id: str
    is_active: bool
    }
  class DeviceStatus {
    id: str
    type: str
    coordinator_id: Union[str,None]
    actions: Dict[str, Action]
    availableBodyGroups: List[BodyGroup]
    }
  class ActionPermissionMessage {
    client_id: str
    }
  class ClaimDeviceMessage {
    client_id: str
    device_id: str
    }
  DeviceStatus --|> Status
  Robot.ActionRuntime -|> Action
  Action "1" o- "1" BodyGroup
  DeviceStatus "1" *- "0..*" BodyGroup
  DeviceStatus "1" *-- "0..*" Action
  Robot.main <... ActionPermissionMessage
  Robot.main <... ClaimDeviceMessage
  Robot.main "1" *- "1" DeviceStatus
  }
@enduml
```
## Class diagram of Client

```plantuml
@startuml Class Diagramm Client
namespace Client {
  class WebController {
    websocket: AsyncServer
    http: FastAPI
    loop: BaseEventLoop
    __router: APIRouter__
    ___runtime: Runtime__
    ' Note: _runtime is class variable 
    sendStatusMsgs() -> None
    _clearcache() -> None
    send_action(response, device_id, action_id, session_data) -> Dict
    _status(response, session_data) -> Dict
    request_device(response, bodyGroup, session_data) -> Dict
    }
  class Runtime {
    websocket: AsyncServer
    http: FastAPI
    loop: BaseEventLoop
    cache: Cache
    clearCache() -> None
    _handle_device_status(device) -> None
    cb_new_session(session) -> None
    }
  class Cache {
    sessions: List[sessionData]
    devices: List[DeviceStatus]
    findSessionByClientId(node_id) -> Session, None
    }
  class Session {
    sessionData: sessionData
    node: CustomNeuropilNode
    }
  class CustomNeuropilNode {
    id: str
    clientStatus: ClientStatus
    evt_device_status: EventHook
    bootstrapper: str
    join()
    _cb_authenticate(node, token) -> bool
    _cb_authorize(node, token) -> bool
    _cb_device_status(node, message) -> None
    sendStatus() -> None
    sendAction(device_id, action_id) -> None
    registerDevice(device) -> None
    requestDevice(bodyGroup) -> None
    }
  Session "1" *- "1" CustomNeuropilNode
  Cache "1" o-- "0..*" Session
  Runtime "1" *- "1" Cache
  WebController "1" *- "0..1" Runtime
  }
namespace Shared {
  class EventHook {
    __handlers:List
    fire() -> None
    }
  class SessionData {
    node_id:str
    name:str
    }
  enum BodyGroup {
    OTHER
    DRIVE
    CLAW
    ARM
    Camera
    }
  class Status {
    time_of_alteration: int
    created_at: int
    }
  class Action {
    id: str
    bodyGroup: BodyGroup
    client_id: str
    is_active: bool
    }
  class DeviceStatus {
    id: str
    type: str
    coordinator_id: Union[str,None]
    actions: Dict[str, Action]
    availableBodyGroups: List[BodyGroup]
    }
  class ClientStatus {
    id: str
    name: str
    }
  class RequestDeviceMessage {
    client_id: str
    bodyGroup: BodyGroup
    }
  DeviceStatus -|> Status
  ClientStatus --|> Status
  Action "1" o- "1" BodyGroup
  RequestDeviceMessage "1" o-- "1" BodyGroup
  DeviceStatus "1" *- "0..*" BodyGroup
  DeviceStatus "1" *-- "0..*" Action
  Client.Session "1" *-- "1" SessionData
  Client.Cache "1" o-- "0..*" DeviceStatus
  Client.CustomNeuropilNode "1" *-- "1" ClientStatus
  Client.CustomNeuropilNode <. EventHook
  Client.CustomNeuropilNode <. RequestDeviceMessage
  }
@enduml
```
## Class diagram of Coordinator

```plantuml
@startuml Class Diagramm Coordinator
namespace Coordinator {
  class WebController {
    websocket: AsyncServer
    http: FastAPI
    loop: BaseEventLoop
    __router: APIRouter__
    ___runtime: Runtime__
    sendStatusMsgs() -> None
    set_device_for_client(response, client_id, device_id, session_data) -> Dict
    _request_device_ownership(response, device_id,session_data) -> None
    _request_device_ownership(response, device_id,owner_id,session_data) -> None
    _set_device_permission_for_client(response, client_id, device_id, bodyGroup,session_data) -> Dict
    set_intent(response, device_id:str,session_data) -> None
    _clearcache() -> None
    status(response, session_data) -> Dict
    }
  class Runtime {
    websocket: AsyncServer
    http: FastAPI
    loop: BaseEventLoop
    cache: Cache
  	clearCache() -> None
    _handle_device_status(node, device) -> None
    _handle_client_status(node, client) -> None
    _handle_request_device(node, msg) -> None
    _handle_request_device_ownership(node, msg) -> None
    _handle_coordinator_status(node, coordinator) -> None
    cb_new_session(data) -> None
    }
  class Cache {
    sessions: List[sessionData]
    devices: List[DeviceStatus]
    clients: List[RuntimeClientStatus]
    coordinators: List[RuntimeClientStatus]
    findSessionByClientId(node_id) -> Session, None
    }
  class Session {
    sessionData: sessionData
    node: CustomNeuropilNode
    }
  class CustomNeuropilNode {
    id: str
    CoordinatorStatus: CoordinatorStatus
    evt_client_status: EventHook
    evt_device_status: EventHook
    evt_coordinator_status: EventHook
    evt_request_device: EventHook
    evt_request_device_ownership: EventHook
    bootstrapper: str
    join() -> None
    _cb_authenticate(node, token) -> bool
    _cb_authorize(node, token) -> bool
    registerDevice(device) -> None
    registerOwner(coordinator_id) -> None
    setActionPermission(device, action, client) -> None
    requestDeviceOwnership(device) -> None
    setDeviceOwnership(device, owner_id) -> None
    sendStatus() -> None
    _cb_device_status(node, message) -> None
    _cb_client_status(node, message) -> None
    _cb_coordinator_status(node, message) -> None
    _cb_request_device(node, message) -> None
    _cb_request_device_ownership(node, message) -> None
    }
  class RuntimeClientStatus {
    device_id: str
    }
  class RuntimeDeviceStatus {
    processed_at: int
    }
  CustomNeuropilNode "1" --* "1" Session
  Cache "1" o-- "0..*" Session
  ' Cache "1..1" o-- "0..*" DeviceStatus
  Cache "1" o-- "0..*" RuntimeClientStatus
  Cache "1" o-- "0..*" RuntimeDeviceStatus
  Runtime "1" *-- "1" Cache
  WebController "1" o- "0..1" Runtime
  }
namespace Shared {
  class EventHook {
    __handlers:List
    fire() -> None
    }
  class SessionData {
    node_id:str
    name:str
    }
  enum BodyGroup {
    OTHER
    DRIVE
    CLAW
    ARM
    Camera
    }
  class Status {
    time_of_alteration: int
    created_at: int
    }
  class Action {
    id: str
    bodyGroup: BodyGroup
    client_id: str
    is_active: bool
    }
  class DeviceStatus {
    id: str
    type: str
    coordinator_id: Union[str,None]
    actions: Dict[str, Action]
    availableBodyGroups: List[BodyGroup]
    }
  class CoordinatorStatus {
    id: str
    name: str
    intendedDeviceId: str
    }
  class ClientStatus {
    id: str
    name: str
    }
  class ActionPermissionMessage {
    client_id: str
    }
  class RequestDeviceMessage {
    client_id: str
    bodyGroup: BodyGroup
    }
  class ClaimDeviceMessage {
    client_id: str
    device_id: str
    }
  DeviceStatus -|> Status
  CoordinatorStatus --|> Status
  ClientStatus --|> Status
  Action "1" o- "1" BodyGroup
  RequestDeviceMessage "1" o- "1" BodyGroup
  DeviceStatus "1" *- "0..*" BodyGroup
  Coordinator.Session "1" *-- "1" SessionData
  DeviceStatus "1" *-- "0..*" Action
  Coordinator.Cache "1" o-- "0..*" DeviceStatus
  Coordinator.RuntimeDeviceStatus --|> DeviceStatus
  Coordinator.CustomNeuropilNode "1" *- "1" CoordinatorStatus
  Coordinator.RuntimeClientStatus --|> ClientStatus
  Coordinator.CustomNeuropilNode <. EventHook
  Coordinator.CustomNeuropilNode <. ActionPermissionMessage
  Coordinator.CustomNeuropilNode <. RequestDeviceMessage
  Coordinator.Runtime <. RequestDeviceMessage
  Coordinator.CustomNeuropilNode <. ClaimDeviceMessage
  Coordinator.Runtime <. ClaimDeviceMessage
  }
@enduml
```
