# Motivation
Goal of this project is to deliver a product capable of communicating over Neuropil, showcase its unique features and make their effect more visible.
We aim more specifically to send a set of messages via Neuropil to a robot, which then reacts appropriately, and use this to both show message delivery and permission management on a physical device.
Thus we have split the project into three parts, each to deliver one aspect of our goal: the Robot is a physical device reacting to incoming Neuropil messages, the Deviceclient is capable of sending simple Instructions to the Robot, while the Coordinator controls the permissions of the Deviceclient.
# Components
```plantuml
() "Neuropil" as Neuropil

package "Coordinator" {
    [Frontend] as CoordinatorFrontend
    () "HTTP" as CoordinatorHTTP
    () "WS" as CoordinatorWS

    CoordinatorFrontend -- CoordinatorHTTP
    CoordinatorFrontend - CoordinatorWS
    package "Backend" as CoordinatorBackend {
        [Node] as CoordinatorNode
        [API] as CoordinatorAPI
        CoordinatorNode - Neuropil
        CoordinatorHTTP - CoordinatorAPI
        CoordinatorWS -- CoordinatorAPI
        CoordinatorAPI <--> CoordinatorNode
    }
}

[Relay] -- Neuropil

package "Deviceclient" {
    [Frontend] as DeviceclientFrontend
    () "HTTP" as DeviceclientHTTP
    () "WS" as DeviceclientWS

    DeviceclientFrontend -- DeviceclientHTTP
    DeviceclientFrontend - DeviceclientWS
    package "Backend" as DeviceclientBackend {
        [Node] as DeviceclientNode
        [API] as DeviceclientAPI
        DeviceclientNode - Neuropil
        DeviceclientHTTP - DeviceclientAPI
        DeviceclientWS -- DeviceclientAPI
        DeviceclientAPI <--> DeviceclientNode
    }
}

package "Robot" {
    [Node] as RobotNode
    [Controller]
    Neuropil -- RobotNode
    [Controller] <-> RobotNode
}
```
Communication between the three packages is handled by their respective Neuropil Nodes, who communicate with each other via the Neuropil protocol.
The Relay is used as a relay between Nodes, transforming the network topology to a star network.
The Controller component in the Robot is used to control the movements of the Robot.
The frontend of both Coordinator and Deviceclient are implemented as a single page Application.

# Workflows
red=Coordinator, blue=Robot, green=Deviceclient
```plantuml
@startuml Permission change
title Permission change

hide unlinked
actor "Coordinator" as Coordinator #red
entity "Coordinator API" as CoordinatorAPI #red
entity "Coordinator Node" as CoordinatorNode #red
entity "Robot" as Robot #blue
entity "Client Node" as ClientNode #green
entity "Client API" as ClientAPI #green
actor "Client" as Client #green

group Set Permission
  Coordinator -> CoordinatorAPI: Send Permission Data
  activate CoordinatorAPI
  CoordinatorAPI -->> Coordinator: Acknowledgement
  deactivate CoordinatorAPI
  CoordinatorAPI ->> CoordinatorNode: initiates transfer of Data with Subject "Set_[ID of Action]"
  CoordinatorNode ->> Robot: Send Data with Neuropil
  Robot ->> Robot: Update new Permissions
end group

group Set Ownership
  Coordinator -> CoordinatorAPI: Send Ownership Data
  activate CoordinatorAPI
  CoordinatorAPI -->> Coordinator: Acknowledgement
  deactivate CoordinatorAPI
  CoordinatorAPI ->> CoordinatorNode: initiates transfer of Data with Subject "[ID of Owner]:device:requst"
  CoordinatorNode ->> Robot: Send Data with Neuropil
  Robot ->> Robot: Update new Ownership
end group
@enduml
```
The process by which both permissions and ownership are changed.

```plantuml
@startuml Use Movement
title Use Movement

hide unlinked
actor "Coordinator" as Coordinator #red
entity "Coordinator API" as CoordinatorAPI #red
entity "Coordinator Node" as CoordinatorNode #red
entity "Robot" as Robot #blue
entity "Client Node" as ClientNode #green
entity "Client API" as ClientAPI #green
actor "Client" as Client #green

Client -> ClientAPI: Send Movement Data
activate ClientAPI
ClientAPI -->> Client: Acknowledgement
deactivate ClientAPI
ClientAPI ->> ClientNode: initiates transfer of Data with Subject "[ID of Action]"
ClientNode ->> Robot: Send Data with Neuropil
Robot ->> Robot: Update new Movement
@enduml
```
The process by which the Deviceclient uses their permission to move the Device.
If the Deviceclient does not have permission their instructions are ignored.

```plantuml
@startuml Request Permission
title Request Permission

hide unlinked
actor "Coordinator" as Coordinator #red
entity "Coordinator API" as CoordinatorAPI #red
entity "Coordinator Node" as CoordinatorNode #red
entity "Robot" as Robot #blue
entity "Client Node" as ClientNode #green
entity "Client API" as ClientAPI #green
actor "Client" as Client #green

Client -> ClientAPI: Send Request Data
activate ClientAPI
ClientAPI -->> Client: Acknowledgement
deactivate ClientAPI
ClientAPI ->> ClientNode: : initiates transfer of Data with Subject "device_request"
ClientNode ->> CoordinatorNode: Send Data with Neuropil
CoordinatorNode ->> CoordinatorAPI: receive transfer of Data
CoordinatorAPI ->> Coordinator: Display Status Object new Request
@enduml
```
The process by which the Deviceclient requests permissions from the owner.

```plantuml
@startuml Refresh Device Data
title Refresh Device Data

hide unlinked
actor "Coordinator" as Coordinator #red
entity "Coordinator API" as CoordinatorAPI #red
entity "Coordinator Node" as CoordinatorNode #red
entity "Robot" as Robot #blue
entity "Client Node" as ClientNode #green
entity "Client API" as ClientAPI #green
actor "Client" as Client #green
!pragma teoz true

Robot ->> Robot: Read Status
Robot ->> CoordinatorNode: Send Data with Neuropil over Subject "device:status"
& Robot ->> ClientNode: Send Data with Neuropil over Subject "device:status"
CoordinatorNode ->> CoordinatorAPI: receive transfer of Data
& ClientNode ->> ClientAPI: receive transfer of Data
CoordinatorAPI ->> Coordinator: Display new Status
& ClientAPI ->> Client: Display new Status
@enduml
```
The process by which the current status of the Device is transferred to the other participants.
This process is executed once every three seconds.