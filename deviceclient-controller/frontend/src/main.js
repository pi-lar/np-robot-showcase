import { createApp } from 'vue'
import { createStore } from 'vuex'

import VueSpectre from '@devindex/vue-spectre';
import axios from 'axios'
import VueSocketIO from 'vue-3-socket.io'

import App from './App.vue'
import './index.css'

const socket_store = createStore();


app =createApp(App)
app.use(VueSpectre, { locale: 'en' });
app.config.globalProperties.axios=axios

app.use(new VueSocketIO({
    debug: true,
    connection: window.location.origin,
    vuex: {
        socket_store,
        actionPrefix: 'SOCKET_',
        mutationPrefix: 'SOCKET_'
    },
    options: { path: "/ws/socket.io" }
}))

app.mount('#app')
