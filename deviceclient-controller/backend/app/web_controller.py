import logging
from uuid import UUID

import asyncio
from socketio import AsyncServer
from fastapi import HTTPException, FastAPI, APIRouter, Response, Depends

from .node import CustomNeuropilNode
from .models import Runtime
from .shared.session import router as session_router, cookie, verifier, evt_new_session, evt_del_session, SessionData
from .shared.models import BodyGroup

logger = logging.getLogger("app")
router = APIRouter()
_runtime:Runtime = None

def init(websocket:AsyncServer, http:FastAPI, loop:asyncio.BaseEventLoop = asyncio.get_running_loop()):
    global _runtime, evt_new_session, evt_del_session
    _runtime = Runtime(websocket, http, loop)
    http.include_router(router)
    http.include_router(session_router)
    _init_websockets(websocket)
    evt_new_session += _runtime.cb_new_session

    loop.create_task(sendStatusMsgs())

async def sendStatusMsgs():
    global _runtime
    while True:
        for session in _runtime.cache.sessions:
            session.node.sendStatus()
            await asyncio.sleep(1)
        await asyncio.sleep(3.1)

@router.get("/clearcache")
async def _clearcache():
    global _runtime
    _runtime.clearCache()


@router.post("/action/{device_id}/{action_id}", dependencies=[Depends(cookie)])
async def send_action(response: Response, device_id:str, action_id:str, session_data: SessionData = Depends(verifier)):
    global _runtime
    node = _runtime.cache.findSessionByClientId(session_data.node_id).node
    node.sendAction(device_id,action_id)
    return {"action":f"try to move device with action {action_id}"}

@router.get("/status", dependencies=[Depends(cookie)])
async def _status(response: Response, session_data: SessionData = Depends(verifier)):
    global _runtime
    node = _runtime.cache.findSessionByClientId(session_data.node_id).node
    status_options = [
        'error',
        'uninitialized',
        'running',
        'stopped',
        'shutdown',
    ]
    ret = {
        "node_id":session_data.node_id,
        "node_status": status_options[node.get_status()],
        "node_has_joined": node.has_joined(),
        "name":session_data.name
    }
    await _runtime.websocket.emit('node_'+session_data.node_id, ret)
    return ret

@router.post("/request_device/{bodyGroup}", dependencies=[Depends(cookie)])
async def request_device(response: Response, bodyGroup:BodyGroup, session_data: SessionData = Depends(verifier)):
    global _runtime
    logging.info(f"{session_data.node_id} requests device access to {bodyGroup.name}")
    node = _runtime.cache.findSessionByClientId(session_data.node_id).node
    node.requestDevice(bodyGroup)
    return {
        "action": f"requested device access to {bodyGroup.name}"
    }
    
def _init_websockets(websocket):
    @websocket.event
    async def _connect(sid, environ):
        global _runtime
        logging.info('connect: '+ sid )
        #await _status()
        
    @websocket.event
    def _disconnect(sid):
        global _runtime
        logging.info('disconnect: '+ sid)        