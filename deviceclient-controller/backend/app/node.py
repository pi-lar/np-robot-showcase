from typing import List, Set
import logging, json

from .shared.event import EventHook
from .shared.models import Action, DeviceStatus, ClientStatus, RequestDeviceMessage, BodyGroup

from neuropil import neuropil, NeuropilNode, np_message, np_token, np_subject

subject_client_status = "urn:io:neuropil:demo:deviceclient:status"
subject_request_device  = "urn:io:neuropil:demo:deviceclient:device_request"
subject_device_status   = "urn:io:neuropil:demo:device:status"

logger = logging.getLogger("app")

class CustomNeuropilNode(NeuropilNode):

    def __init__(self, bootstrapper:str, **kwargs):
        super().__init__(log_level=neuropil.LOG_NONE, **kwargs)
        self.set_authenticate_cb(self._cb_authenticate)
        self.set_authorize_cb(self._cb_authorize)
        
        mxp = self.get_mx_properties(subject_client_status)
        mxp.ackmode = neuropil.NP_MX_ACK_NONE
        mxp.role = neuropil.NP_MX_PROVIDER
        mxp.intent_ttl = 3600
        mxp.intent_update_after = 60
        mxp.max_retry = 3
        mxp.apply()

        mxp = self.get_mx_properties(subject_device_status)
        mxp.ackmode = neuropil.NP_MX_ACK_NONE
        mxp.role = neuropil.NP_MX_CONSUMER
        mxp.intent_ttl = 3600
        mxp.intent_update_after = 60
        mxp.max_retry = 3
        mxp.apply()
        self.set_receive_cb(subject_device_status, self._cb_device_status)
        self.evt_device_status = EventHook()

        mxp = self.get_mx_properties(subject_request_device)
        mxp.ackmode = neuropil.NP_MX_ACK_DESTINATION
        mxp.role = neuropil.NP_MX_PROVIDER
        mxp.intent_ttl = 3600
        mxp.intent_update_after = 60
        mxp.max_retry = 3
        mxp.apply()

        self.bootstrapper = bootstrapper
        logger.info(f"Running deviceclient neuropil node: {self.get_address()}")#[:64]
        self.run(0)
        self.join()
        self.id = self.get_address()[:64]

        self.clientStatus:ClientStatus

    def join(self):
        if not self.has_joined():
            super().join(self.bootstrapper)
            logger.info(f"Joining {self.bootstrapper}")

    def _cb_authenticate(self, node:NeuropilNode, token:np_token):
        logger.info("Authenticate " + token.subject)
        return True

    def _cb_authorize(self, node:NeuropilNode, token:np_token):
        logger.info("Authorize " + token.subject)
        return True

    def _cb_device_status(self, node:NeuropilNode, message:np_message):
        self.evt_device_status.fire(DeviceStatus.parse_raw(message.raw().decode("utf-8")))
    
    def sendStatus(self):
        msg = self.clientStatus
        self.send(subject_client_status, msg.json())
    
    def sendAction(self, device_id, action_id):        
        self.send(f"urn:io:neuropil:demo:device:{device_id}:{action_id}", "<tbd>")

    def registerDevice(self, device:DeviceStatus):
        for action_id in device.actions:
            mxp = self.get_mx_properties(np_subject.generate(f"urn:io:neuropil:demo:device:{device.id}:{action_id}"))
            mxp.ackmode = neuropil.NP_MX_ACK_DESTINATION
            mxp.role = neuropil.NP_MX_PROVIDER
            mxp.intent_ttl = 120
            mxp.intent_update_after = 60
            mxp.max_retry = 3
            mxp.apply()

    def requestDevice(self, bodyGroup:BodyGroup):
        msg = RequestDeviceMessage(client_id=self.id, bodyGroup=bodyGroup)
        self.send(subject_request_device, msg.json())
