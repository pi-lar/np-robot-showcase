import logging, os
from typing import List, Set, Dict, Optional, Union

from pydantic import BaseModel
from socketio import AsyncServer
from fastapi import FastAPI
import asyncio

from .shared.models import DeviceStatus, ClientStatus
from .shared.session import SessionData
from .node import CustomNeuropilNode

logger = logging.getLogger("app")
class Session(BaseModel):
    sessionData:SessionData
    node:CustomNeuropilNode
    class Config:
        arbitrary_types_allowed = True

class Cache():
    sessions:List[Session] = []
    devices:Dict[(str,DeviceStatus)] = {}

    def findSessionByClientId(self,node_id:str) -> Union[Session,None]:
        for session in self.sessions:
            if session.sessionData.node_id == node_id:
                return session
        return None

    def __del__(self):
        for session in self.sessions:
            session.node.shutdown(False)

class Runtime():
    def __init__(self, websocket:AsyncServer, http:FastAPI, loop:asyncio.BaseEventLoop):
        self.websocket = websocket
        self.http = http
        self.loop = loop
        self.cache = Cache()

    def clearCache(self):
        self.cache = Cache()

    def _handle_device_status(self, device:DeviceStatus):        
        cached_device:DeviceStatus = None
        force = False
        try:
            cached_device = self.cache.devices[device.id]
        except KeyError:
            cached_device = device
            force = True
        
        self.loop.create_task(self.websocket.emit('add_new_device', device.dict()))
        if force or cached_device.json() != device.json():
            for action_id in device.actions.keys():
                action = device.actions[action_id]
                cached_action = cached_device.actions[action_id]
                if force or cached_action.json() != action.json():
                    update_action_data = action.dict()
                    update_action_data["device_id"] = device.id
                    if cached_action.client_id and cached_action.client_id !=  action.client_id:
                        self.loop.create_task(self.websocket.emit('rm_action'+cached_action.client_id, update_action_data))
                    if action.client_id:
                        self.loop.create_task(self.websocket.emit('update_action'+action.client_id, update_action_data))
                        session = self.cache.findSessionByClientId(action.client_id)
                        is_new_device = not device.id in self.cache.devices
                        if is_new_device:
                            session.node.registerDevice(device)

        self.cache.devices[device.id] = device

    def cb_new_session(self, session:SessionData):
        node = CustomNeuropilNode(
            port = 9005,
            proto = "pas4",
            host="0.0.0.0",
            bootstrapper = os.getenv("NP_BOOTSTRAPPER")
        )
        node.evt_device_status += self._handle_device_status
        session.node_id = node.id
        node.clientStatus = ClientStatus(id=node.id, name=session.name)
        self.cache.sessions.append(Session(sessionData=session,node=node))
        

