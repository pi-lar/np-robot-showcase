import os, logging, sys
from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
import socketio

from app.node import CustomNeuropilNode
from app.web_controller import init

logger = logging.getLogger("uvicorn.error")

logger = logging.getLogger("app")
streamHandler = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter("%(levelname)s %(asctime)s - %(message)s")
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)
logger.setLevel(logging.INFO)
logger.propagate = False

origins = os.environ.get("CONTROLLER_ORIGINS","*").split(" ")

sio = socketio.AsyncServer(async_mode='asgi', cors_allowed_origins=[])

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

controller = init(sio, app)
app.mount("/ws",socketio.ASGIApp(socketio_server=sio, socketio_path="socket.io"))

#from routes import init_routes
#init_routes(app, sio)

app.mount("/", StaticFiles(directory="dist",html=True), name="static")


